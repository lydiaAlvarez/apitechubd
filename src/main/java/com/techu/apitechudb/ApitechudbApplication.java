package com.techu.apitechudb;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.ShopModel;
import com.techu.apitechudb.models.UserModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;

@SpringBootApplication
public class ApitechudbApplication {
	public static ArrayList<ProductModel> productModels;
    public static ArrayList<UserModel> userModels;
	public static ArrayList<ShopModel> shopModels;

	public static void main(String[] args) {

		SpringApplication.run(ApitechudbApplication.class, args);

		ApitechudbApplication.productModels = ApitechudbApplication.getTestData();
		ApitechudbApplication.userModels = ApitechudbApplication.getUserData();
		ApitechudbApplication.shopModels = ApitechudbApplication.getShopTester();
	}

	private static ArrayList<ProductModel> getTestData(){

		ArrayList<ProductModel> productModels = new ArrayList<>();

		productModels.add(
				new ProductModel(
						"1",
						"Producto 1",
						100
				)
		);

		productModels.add(
				new ProductModel(
						"2",
						"Producto 2",
						200
				)
		);

		productModels.add(
				new ProductModel(
						"3",
						"Producto 3",
						300
				)
		);

		return productModels;
	}

	private static ArrayList<UserModel> getUserData() {
		ArrayList<UserModel> userModels = new ArrayList<>();
		// creamos los usuarios
		userModels.add(
				new UserModel(
						"1",
						"Jaume",
						26
				)
		);

		userModels.add(
				new UserModel(
						"2",
						"Fernando",
						19
				)
		);

		userModels.add(
				new UserModel(
						"3",
						"Ana Maria",
						30
				)
		);

		userModels.add(
				new UserModel(
						"4",
						"Luisa",
						58
				)
		);

		userModels.add(
				new UserModel(
						"5",
						"Adara",
						28
				)
		);

		userModels.add(
				new UserModel(
						"6",
						"Iris",
						28
				)
		);
		userModels.add(
				new UserModel(
						"7",
						"Ainhoa",
						30
				)
		);

		userModels.add(
				new UserModel(
						"8",
						"Douglas",
						26
				)
		);

		return userModels;
	}

	// Añadimos los valores para la compra
	private static ArrayList<ShopModel> getShopTester(){
		return new ArrayList<>();
	}
}
