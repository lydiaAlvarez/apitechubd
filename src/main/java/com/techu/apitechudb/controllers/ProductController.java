package com.techu.apitechudb.controllers;


import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class ProductController {

//    @Autowired
//    ProductService productRepository;
    @Autowired
    ProductService productService;


//    @GetMapping("/products")
//    public List<ProductModel> getProducts(){
//        System.out.println("getProducts");
//
//
//        return this.productService.findAll();
//    }

    @GetMapping("/products")
    public ResponseEntity<List<ProductModel>> getProducts(){
        System.out.println("ResponseEntity");

        return new ResponseEntity<>(
                this.productService.findAll(),
                HttpStatus.OK
                );
    }

//    @PostMapping("/products")
//    public ProductModel addProduct(@RequestBody ProductModel product){
//        System.out.println("addProduct");
//        System.out.println("La id a crear es: " + product.getId());
//        System.out.println("La desc a crear es: " + product.getDesc());
//        System.out.println("El importe a crear es: " + product.getPrice());
//
//        return new ProductModel();
//    }

//    @PostMapping("/products")
//    public ProductModel add(ProductModel product){
//        System.out.println("add en productService");
//
//        return this.productRepository.add(product);
//    }

    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel product){
        System.out.println("addProduct");
        System.out.println("La id a crear es: " + product.getId());
        System.out.println("La desc a crear es: " + product.getDesc());
        System.out.println("El importe a crear es: " + product.getPrice());

        return new ResponseEntity<>(
                this.productService.add(product),
                HttpStatus.CREATED
        );
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getProductsById(@PathVariable String id){
        System.out.println("byID");
        System.out.println("La id a buscar es: " + id);

        Optional<ProductModel> result = this.productService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Producto no encontrado" ,
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

//    @PutMapping("/products/{id}")
//    public ResponseEntity<Object> updateProductById(@PathVariable String id){
//        System.out.println("byID");
//        System.out.println("La id a buscar es: " + id);
//
//        Optional<ProductModel> result = this.productService.findById(id);
//
//        return new ResponseEntity<>(
//                result.isPresent() ? result.get() : "Producto no encontrado" ,
//                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
//        );
//    }

    @PutMapping("/products/{id}")
    public ResponseEntity<ProductModel> updateProduct(@RequestBody ProductModel product, @PathVariable String id){
        System.out.println("updateProduct");
        System.out.println("Se va a actualizar la ID del producto " + id);
        System.out.println("la nueva ID es: " + product.getId());
        System.out.println("la nueva desc es: " + product.getDesc());
        System.out.println("el nuevo precio es: " + product.getPrice());

        return new ResponseEntity<>(
                this.productService.update(product), HttpStatus.OK
        );
    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity<String> deleteProducts(@PathVariable String id) {
        System.out.println("deleteProduct");
        System.out.println("la ID del producto a borrar es: " + id);

        boolean deleteProduct = this.productService.delete(id);

        return new ResponseEntity<>(
                deleteProduct ? "Poducto encontrado" : "Producto no encontrado",
                deleteProduct ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }


// Enunciado práctica
//
//
//    La práctica consiste en crear el recurso usuario: peticiones
//    REST y código necesario.
//
//    Clases necesarias:
//
//            · UserModel
//                  id tipo String (identificador)
//                  name tipo String
//                  age tipo int
//            · UserRepository
//            · UserService
//            · UserController
//
//    Consultar anotaciones de las clases de Product como referencia y adaptarlas en caso de ser necesario.
//
//    Definiendo una combinación de URL y método http acorde con REST, preparar
//    las siguientes peticiones:
//
//            · Obtener todos los usuarios, pudiendo seleccionar solo aquellos con una edad determinada pasada por parámetro.
//    NOTA - Utilizar un parámetro de Query String opcional para pedir obtener solo aquellos usuarios con una edad concreta.
//            · Obtener un usuario en base a su id.
//            · Crear un usuario.
//·             Actualizar completamente un usuario (menos la id) en base a su id.
//            · Borrar un usuario en base a su id.
}
