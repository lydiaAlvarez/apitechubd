package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.ShopModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.ServiceResponse;
import com.techu.apitechudb.services.ShopService;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("apitechudb/practicaCompra")
public class ShopController {

    @Autowired
    ShopService shopService;

    // Consulta de compras
    @GetMapping("/compras")
    public ResponseEntity<List<ShopModel>> getShops(){
        System.out.println("ResponseEntity de getShops");

        return new ResponseEntity<>(
                this.shopService.findShop(),
                HttpStatus.OK
        );
    }

    @PostMapping("/compras")
    public ResponseEntity<ShopModel> addShop (@RequestBody ShopModel purchase){
        System.out.println("addShop");
        System.out.println("");

        ServiceResponse serviceResponse = this.shopService.addShop(purchase);
        return new ResponseEntity<>(
                this.shopService.addShop(purchase).getPurchase(),
                this.shopService.addShop(purchase).getResponseStatus()
        );
    }
}
