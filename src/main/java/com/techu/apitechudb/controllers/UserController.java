package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.lang.reflect.Array;
import java.util.List;
import java.util.Map;
import java.util.Optional;


@RestController
@RequestMapping("apitechudb/practicaUser")
public class UserController {

    @Autowired
    UserService userService;

    // Consulta de usuarios por edad
    @GetMapping(path = {"/usuarios", "/usuarios/{edad}"})
    public ResponseEntity<List<UserModel>> getUsers(@RequestParam(name="edad", required = false, defaultValue = "0")
           int ageFilter ){
        System.out.println("ResponseEntity de getUsers");

        return new ResponseEntity<>(
                this.userService.findUser(ageFilter),
                HttpStatus.OK
        );
    }

//    // Consulta de usuarios
//    @GetMapping("/usuarios")
//    public ResponseEntity<List<UserModel>> getUsers(){
//        System.out.println("ResponseEntity de getUsers");
//
//
//        return new ResponseEntity<>(
//                this.userService.findUser(),
//                HttpStatus.OK
//        );
//    }

    // Consulta de usuarios por ID
    @GetMapping("/usuarios/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id){
        System.out.println("buscar usuario por ID");
        System.out.println("La id a buscar es: " + id);

        Optional<UserModel> user = this.userService.findById(id);

        return new ResponseEntity<>(
                user.isPresent() ? user.get() : "Usuario no encontrado" ,
                user.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    // Actualizar el registro
    @PutMapping("/usuarios/{id}")
    public ResponseEntity<UserModel> updateUser (@RequestBody UserModel usuario, @PathVariable String id){
        System.out.println("updateProduct");
        System.out.println("Se va a actualizar el usuario con ID: " + id);
        System.out.println("El nuevo nombre es: " + usuario.getName());
        System.out.println("La edad actaulizada es: " + usuario.getAge());

        return new ResponseEntity<>(
                this.userService.update(usuario), HttpStatus.OK
        );
    }

    @DeleteMapping("/usuarios/{id}")
    public ResponseEntity<String> deleteUsers (@PathVariable String id) {
        System.out.println("deleteUsers");
        System.out.println("la ID del usuario a borrar es: " + id);

        boolean deleteUser = this.userService.delete(id);

        return new ResponseEntity<>(
                deleteUser ? "Usuario encontrado" : "Usuario no encontrado",
                deleteUser ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PostMapping("/usuarios")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel usuario){
        System.out.println("add Usuario");
        System.out.println("La id a crear es: " + usuario.getId());
        System.out.println("El nombre del usuario es: " + usuario.getName());
        System.out.println("La edad del usuario es: " + usuario.getAge());

        return new ResponseEntity<>(
                this.userService.add(usuario),
                HttpStatus.CREATED
        );
    }
}
