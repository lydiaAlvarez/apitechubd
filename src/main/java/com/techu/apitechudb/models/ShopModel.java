package com.techu.apitechudb.models;

import com.techu.apitechudb.ApitechudbApplication;

import java.util.ArrayList;
import java.util.Map;

public class ShopModel {
//    · Crear una compra.
//
//      Requisitos:
//          · Comprobar que la id de usuario se corresponda con un usuario.
//	        · Comprobar que no haya una compra con esa id.
//	        · Comprobar que todos los productos existen.
//
//    Ante el fallo de cualquiera de estas comprobaciones el sistema devolverá un mensaje apropiado y un código
//    http de estado apropiado.
//    Vale con devolverlo cuando falle solo una, si fallan varias no es necesario "agrupar" mensajes.
//          · Una vez pasen todas las comprobaciones, el sistema calculará el total de la 	compra en base al precio
//            almacenado de cada uno de los productos.
//
//          · Obtener todas las compras.
//    CONSEJO - Primero hacer funcionar una inserción básica de compra, a partir
//    de ahí añadir el resto de requisitos.
//
//    PISTA - Si la gestión del mensaje de respuesta al usuario, según falle algunade las comprobaciones,
//            se hace compleja, considerar una respuesta alternativa del servicio para "agrupar" la información que
//            irá al controlador.

    private String id;
    private String idUsuario;
    private Map<String, Integer> compraProductos;
    private float importeCompra;

    public ShopModel() {
    }

    public ShopModel(String id, String idUsuario, Map<String, Integer> compraProductos, float importeCompra) {
        this.id = id;
        this.idUsuario = idUsuario;
        this.compraProductos = compraProductos;
        this.importeCompra = importeCompra;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Map<String, Integer> getCompraProductos() {
        return compraProductos;
    }

    public void setCompraProductos(Map<String, Integer> compraProductos) {
        this.compraProductos = compraProductos;
    }

    public float getImporteCompra() {
        return importeCompra;
    }

    public void setImporteCompra(float importeCompra) {
        this.importeCompra = importeCompra;
    }
}
