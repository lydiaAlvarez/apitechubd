package com.techu.apitechudb.models;

public class UserModel {
    // creamos las variables que nos han pedido en el enunciado de la práctica
    private String id;
    private String name;
    private int age;

    // añadimos las funciones para obtener los datos
    public UserModel(String id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }
    // obtener id
    public String getId() {

        return this.id;
    }
    // devolvemos id
    public void setId(String id) {

        this.id = id;
    }
    // obtener nombre
    public String getName() {

        return this.name;
    }
    // devolvemos nombre
    public void setName(String name) {

        this.name = name;
    }
    // obtenemos la edad
    public int getAge() {

        return this.age;
    }
    // devolvemos la edad
    public void setAge(int age) {

        this.age = age;
    }
}
