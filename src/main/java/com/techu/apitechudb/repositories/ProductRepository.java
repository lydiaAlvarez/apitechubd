package com.techu.apitechudb.repositories;

import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.models.ProductModel;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class ProductRepository {

    public List<ProductModel> findAll(){

        return ApitechudbApplication.productModels;
    }

    public ProductModel save(ProductModel product){
        System.out.println("Save en productRepository");

        ApitechudbApplication.productModels.add(product);
        return  product;
    }

    public Optional<ProductModel> findById(String id){
        System.out.println("findById en ProducRepository");
        Optional<ProductModel> result = Optional.empty();

        for(ProductModel productInList : ApitechudbApplication.productModels){
            if(productInList.getId().equals(id)){
                System.out.println("Producto encontrado con ID: " + id);
                // le decimos que llene los datos con estos valores encontrados
                result = Optional.of(productInList);
            }
        }
        return result;
    }

    public ProductModel update(ProductModel product){
        System.out.println("Update en PRoductRepository");

        Optional<ProductModel> productToUpdate = this.findById(product.getId());

        if (productToUpdate.isPresent() == true){
            System.out.println("Producto encontrado");

            ProductModel productFromList = productToUpdate.get();
            productFromList.setDesc(product.getDesc());
            productFromList.setPrice(product.getPrice());
        }

        return product;
    }

    public void delete(ProductModel product){
        System.out.println("delete en repoository");
        System.out.println("borrando producto");
        ApitechudbApplication.productModels.remove(product);
    }
}
