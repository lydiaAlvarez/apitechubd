package com.techu.apitechudb.repositories;

import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.models.ShopModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.models.ProductModel;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class ShopRepository {
    // Lista de todas las compras
    public List<ShopModel> findShop(){
        System.out.println("findShop en ShopRepository");

        return ApitechudbApplication.shopModels;
    }
    // Lista de todas las compras
    public ShopModel save(ShopModel purchase){
        System.out.println("findShop en ShopRepository");
         ApitechudbApplication.shopModels.add(purchase);
        return purchase;
    }

    // Busqueda por ID de usuario
    public Optional<ShopModel> findCompraById(String id){
        System.out.println("findCompraById en ShopRepository");
        Optional<ShopModel> purchase = Optional.empty();

        for(ShopModel shopInList : ApitechudbApplication.shopModels){
            if(shopInList.getIdUsuario().equals(id)){
                System.out.println("Compra encontrada con ID: " + id);
                purchase = Optional.of(shopInList);
            }
        }
        return purchase;
    }

}