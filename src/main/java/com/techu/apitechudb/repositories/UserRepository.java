package com.techu.apitechudb.repositories;

import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.models.UserModel;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class UserRepository {

    public List<UserModel> findUser(){
        System.out.println("findAll en ProductRepository");

        return ApitechudbApplication.userModels;
    }

//    public ArrayList<UserModel> findByAge(int age){
//        System.out.println("find by age en User Repository");
//        ArrayList<UserModel> result = new ArrayList<>();
//
//        for (UserModel userInList : ApitechudbApplication.userModels){
//            if (userInList.getAge() == age){
//                System.out.println("Usuario encontrado");
//                result.add(userInList);
//            }
//        }
//        return result;
//    }

    public Optional<UserModel> findById(String id){
        System.out.println("findById en UserRepository");
        Optional<UserModel> user = Optional.empty();

        for(UserModel userInList : ApitechudbApplication.userModels){
            if(userInList.getId().equals(id)){
                System.out.println("Usuario encontrado con ID: " + id);
                user = Optional.of(userInList);
            }
        }
        return user;
    }

    public UserModel update(UserModel usuario){
        System.out.println("Update en UserRepository");

        Optional<UserModel> userToUpdate = this.findById(usuario.getId());

        if (userToUpdate.isPresent() == true){
            System.out.println("Usuario encontrado");

            UserModel userFromList = userToUpdate.get();
            userFromList.setName(usuario.getName());
            userFromList.setAge(usuario.getAge());
        }

        return usuario;
    }

    public void delete(UserModel user){
        System.out.println("delete en repoository");
        System.out.println("borrando usuario...");
        ApitechudbApplication.userModels.remove(user);
    }

    public UserModel save(UserModel user){
        System.out.println("Save en userRepository");

        ApitechudbApplication.userModels.add(user);
        return  user;
    }


}
