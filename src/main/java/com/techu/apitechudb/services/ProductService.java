package com.techu.apitechudb.services;

import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    public List<ProductModel> findAll(){
        System.out.println("findAll en ProductService");

        return this.productRepository.findAll();
    }

    public ProductModel add(ProductModel product){
        System.out.println("Add en productRepository");

        return  this.productRepository.save(product);
    }

    public Optional<ProductModel> findById(String id){
        System.out.println("findById en ProductService");

        return this.productRepository.findById(id);
    }

    public ProductModel update(ProductModel productModel){
        System.out.println("Update en ProductService");

        return  this.productRepository.update(productModel);
    }

    public boolean delete(String id){
        System.out.println("Delete en ProductService");

        boolean result = false;

        Optional<ProductModel> productToDelete = this.findById(id);

        if (productToDelete.isPresent() == true){
            result = true;
            this.productRepository.delete(productToDelete.get());
        }

        return result;

//        ApitechudbApplication.productModels.remove();
    }
}
