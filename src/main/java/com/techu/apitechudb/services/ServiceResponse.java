package com.techu.apitechudb.services;

import com.techu.apitechudb.models.ShopModel;
import org.springframework.http.HttpStatus;

public class ServiceResponse {
    private String msg;
    private ShopModel purchase;
    private HttpStatus responseStatus;

    public ServiceResponse() {

    }

    public ServiceResponse(String msg, ShopModel purchase, HttpStatus responseStatus) {
        this.msg = msg;
        this.purchase = purchase;
        this.responseStatus = responseStatus;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ShopModel getPurchase() {
        return purchase;
    }

    public void setPurchase(ShopModel purchase) {
        this.purchase = purchase;
    }

    public HttpStatus getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(HttpStatus responseStatus) {
        this.responseStatus = responseStatus;
    }
}
