package com.techu.apitechudb.services;

import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.models.ShopModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.repositories.ShopRepository;
import com.techu.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;


@Service
public class ShopService {
    @Autowired
    ShopRepository shopRepository;

    @Autowired
    UserService userService;

    @Autowired
    ProductService productService;

    // buscamos compra
    public List<ShopModel> findShop() {
        System.out.println("finShop en ShopService");
        return this.shopRepository.findShop();
    }

    // añadir compra
    public ServiceResponse addShop(ShopModel purchase) {
        System.out.println("addShop en ShopService");

        ServiceResponse result = new ServiceResponse();
        result.setPurchase(purchase);

        if (this.userService.findById(purchase.getIdUsuario()).isPresent() == false) {
            System.out.println("usuario no encontrado");
            result.setMsg("usuario no encontrado");
            result.setResponseStatus(HttpStatus.BAD_REQUEST);

            return result;
        }

        if (this.getById(purchase.getId()).isPresent() == true){
            System.out.println("existe una compra con ese ID");
            result.setMsg("Ya existe una compra con ese ID");
            result.setResponseStatus(HttpStatus.BAD_REQUEST);

            return result;
        }

        for(Map.Entry<String, Integer> compraProductos : purchase.getCompraProductos().entrySet()){
            if (this.productService.findById(compraProductos.getKey()).isPresent() == false){
                System.out.println("el producto con la id: " + compraProductos.getKey() +
                        " no se encuentra en el sistema");
                result.setMsg("El producto no se encuentra en el sistema");
                result.setResponseStatus(HttpStatus.BAD_REQUEST);

                return result;
            }
        }

        float amount = 0;
        for(Map.Entry<String, Integer> compraProductos : purchase.getCompraProductos().entrySet()){
            if (this.productService.findById(compraProductos.getKey()).isPresent() == false){
                System.out.println("el producto con la id: " + compraProductos.getKey() +
                        " no se encuentra en el sistema");
                result.setMsg("El producto no se encuentra en el sistema");
                result.setResponseStatus(HttpStatus.BAD_REQUEST);

                return result;
            } else {
                amount +=
                (this.productService.findById(compraProductos.getKey()).get().getPrice()
                             * compraProductos.getValue());
            }
        }

        purchase.setImporteCompra(amount);

        this.shopRepository.save(purchase);
        result.setMsg("Compra añadida correctamente");
        result.setResponseStatus(HttpStatus.OK);
        return result;
    }

    public Optional<ShopModel> getById(String id){
        System.out.println("get by id en ShopService");
        System.out.println("la id es: " + id);

        return this.shopRepository.findCompraById(id);
    }
}
