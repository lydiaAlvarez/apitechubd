package com.techu.apitechudb.services;

import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    // buscamos usuario
    public List<UserModel> findUser(int ageFilter) {
        System.out.println("findAll en UserService");
        if (ageFilter <= 0) {
            return this.userRepository.findUser();
        }

        ArrayList<UserModel> result = new ArrayList<>();

        for(UserModel userInList : ApitechudbApplication.userModels){
            if (userInList.getAge() == ageFilter){
                result.add(userInList);
            }
        }
        return result;
    }

    // buscamos usuario por la edad
//    public List<UserModel> findByAge(int age){
//        System.out.println("findByAge en UserService");
//
//        return this.userRepository.findByAge(age);
//    }

    // buscamos usuario por la ID
    public Optional<UserModel> findById(String id){
        System.out.println("findById en UserService");

        return this.userRepository.findById(id);
    }

    // actualizar usuario
    public UserModel update(UserModel userModel){
        System.out.println("Update en ProductService");

        return  this.userRepository.update(userModel);
    }

    // borrar usuario
    public boolean delete(String id){
        System.out.println("Delete en UserService");

        boolean resultado = false;

        Optional<UserModel> userToDelete = this.findById(id);

        if (userToDelete.isPresent() == true){
            resultado = true;
            this.userRepository.delete(userToDelete.get());
        }

        return resultado;
    }

    // crear usuario
    public UserModel add(UserModel user){
        System.out.println("Add en userRepository");

        return  this.userRepository.save(user);
    }
}
